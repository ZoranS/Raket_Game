﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EindScherm : MonoBehaviour {

    [SerializeField]
    private Image m_BackGround;

	// Use this for initialization
	void Start () {
        m_BackGround.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.A))
        {
            m_BackGround.enabled = true;
        }
	}
}
